import vk
from googleapiclient.discovery import build
import facebook
from django.utils.translation import ugettext as _
from .tools import get_sex

def google_plus_search(string_name, count):
    if string_name:
        service = build('plus', 'v1', developerKey='AIzaSyC3P4APPgx2Qew-tM6f98yXyDuJAMS0ROQ')
        people_resource = service.people()
        people_document = people_resource.search(
            maxResults=count, query=string_name).execute()
        users_list = []
        if 'items' in people_document:
            for user in people_document['items']:
                users = {}
                users['name'] = user['displayName']
                users['page'] = user['url']
                users['api'] = 'GooglePlus'
                users['avatar'] = user['image']['url']
                users_list.append(users)
        if users_list:
            return users_list
        else:
            return None
    else:
        return None


def vk_search(string_name, count, age_from, age_to, sex=0):
    if age_from == '':
        age_from = 0
    else:
        age_from = int(age_from)
    if age_to == '':
        age_to = 100
    else:
        age_to = int(age_to)
    sex = int(sex)

    session = vk.Session()
    vkapi = vk.API(session,
                   access_token='b5ab20cde56735b15949c2cf66fa9b4465e19f793f4791d5e2b76fa3f6c0cbfc2c79dd6e84b7f6c48eaa7')
    if string_name:
        users = vkapi.users.search(q=string_name,
                                   age_from=age_from,
                                   age_to=age_to,
                                   sex=sex,
                                   count=count,
                                   fields=['photo_50', 'sex',
                                           'bdate', 'domain'])
    else:
        users = vkapi.users.search(age_from=age_from,
                                   age_to=age_to,
                                   sex=sex,
                                   count=count,
                                   fields=['photo_50', 'sex', 'bdate', 'domain'])
    users_list = []
    users = users[1:]
    for user in users:
        vk_users = {}
        vk_users['first_name'] = user['first_name']
        vk_users['last_name'] = user['last_name']
        if 'bdate' in user:
            vk_users['bdate'] = user['bdate']
        else:
            vk_users['bdate'] = _('Birthday not set')
        vk_users['sex'] = get_sex(user['sex'])
        vk_users['photo'] = user['photo_50']
        vk_users['domain'] = user['domain']
        vk_users['api'] = 'VK'
        users_list.append(vk_users)

    if users_list:
        return users_list
    else:
        return None



def fb_search(string_name, count):
    token = 'CAAOMBFWShesBANzB37SZCg0d6vyL9ZCKEsnjgb8ykPGBG6eyP8UtE3ZBqwSwMKmlngZCXQtNZBuzqJWBkrDArtkZBz9kiltTyZC02oJnGOHdcKvZC7HVn0QUjpgCZCRnUc58mKaZAibC4BhsvDAuTs7HQuBndKGg68AlCYrD6byE9QZBc47Dx22TVVX'
    graph = facebook.GraphAPI(token)
    count = int(count)

    post = graph.request('search', {'q': string_name, 'limit': count, 'type': 'user', 'fields':'id, name, picture, link'})
    all_users = post['data']
    users_list = []
    for user in all_users:
        fb_user = {}
        fb_user['name'] = user['name']
        fb_user['picture'] = user['picture']['data']['url']
        fb_user['link'] = user['link']
        fb_user['api'] = 'facebook'
        users_list.append(fb_user)
    return users_list
