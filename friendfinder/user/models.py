from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class MyUser(AbstractUser):
    class Meta():
        db_table = 'myuser'
    #username = models.CharField(max_length=70)
    #email = models.EmailField()
    #password = models.CharField(max_length=70)
    #first_name = models.CharField(max_length=70)
    #last_name = models.CharField(max_length=70)
    image = models.ImageField(upload_to = 'photos/', default = 'photos/no-img.jpg')
