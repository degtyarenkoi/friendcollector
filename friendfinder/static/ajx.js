function addFriend(e) {
    var form = $(e).parent();
    var name = form.find('input[name=name]').val();
    var api = form.find('input[name=api]').val();
    var link = form.find('input[name=link]').val();
    var picture = form.find('input[name=picture]').val();
    var csrf = $('input[name=csrfmiddlewaretoken]').val();
    var $toastContent = $('<span>Already in your firends</span>');
    var $successContent = $('<span>Success added</span>');

    var data = {name:name, api:api, link:link, picture:picture, csrfmiddlewaretoken:csrf}
    $.ajax({
	data:data,
	url:'/receiver/',
	type:'GET',
	success:function(message) {
	    if (message == 'EXIST'){
            $(e).addClass('disabled');
		    Materialize.toast($toastContent, 3000);
	    }
	    else{
    		$(e).addClass('disabled');
            Materialize.toast($successContent, 3000);
	    }
	}
    });
}
