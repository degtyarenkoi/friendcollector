from django.conf.urls import url, include
from . import views

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^$', views.main),
    url(r'^addfriend/$', views.addfriend, name="friend_add"),
    url(r'^deletefriend/(?P<friend_id>\d+)/$', views.deletefriend),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns() + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
    # staticfiles_urlpatterns() will return the proper URL pattern for serving static files to your already defined pattern list.
    # static() - Helper function to return a URL pattern for serving files in debug mode