from __future__ import unicode_literals
from django.db import models
#from django.contrib.auth.models import User
from user.models import MyUser

# Create your models here.


class Friend(models.Model):
    class Meta():
        db_table = 'friend'
    name = models.CharField(max_length=70)
    api = models.CharField(max_length=20)
    page = models.URLField(max_length=200)
    image = models.URLField(max_length=200)
    user = models.ForeignKey(MyUser)
