from django.conf.urls import url, include
from user import views

urlpatterns = [
    # url(r'^$', views.index),
    url(r'^login/$', views.login_view, name="login"),
    url(r'^logout/$', views.logout_view, name="logout"),
    url(r'^user_add/$', views.user_add, name="user_add"),
]
