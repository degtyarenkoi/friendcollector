from django.shortcuts import render, render_to_response
from django.contrib.auth import authenticate, login, logout, get_user
from django.shortcuts import redirect
#from django.contrib.auth.models import User
from models import MyUser
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse

from .forms import UserForm
from .forms import LoginForm


# Create your views here.

def login_view(request):
    logout(request)

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')
    else:
        form = LoginForm()

    return render(request, 'login.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('/')

def user_add(request):
    if request.method == "POST":
        form = UserForm(request.POST, request.FILES)
        if form.is_valid():
            new_user = MyUser.objects.create_user(**form.cleaned_data)
            login_view(request)
            return HttpResponseRedirect('/')
    else:
        form = UserForm()

    return render(request, 'sign_up.html', {'form': form})
