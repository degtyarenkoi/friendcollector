#from django.contrib.auth.models import User
from models import MyUser
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


class UserForm(forms.ModelForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = MyUser
        fields = ('username', 'email', 'password', 'first_name', 'last_name', 'image')


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())
