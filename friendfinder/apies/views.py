from django.shortcuts import render, HttpResponse
from .tools import get_sex, get_string_name
from .plagins import google_plus_search, vk_search, fb_search
from .twitter_api import twitter_search
from django.contrib.auth import get_user
from django.utils.translation import ugettext as _
from userpage.models import Friend


def index(request):
    if request.method == 'POST':
        post = request.POST.copy()
        string_name = post['q']
        count = post['count']
        google, vk, twitter, linked, fb = [], [], [], [], []
        if 'google' in post:
            google = google_plus_search(string_name, count)
        if 'vk' in post:
            vk = vk_search(string_name, count, post['age'], post['age'], post['sex'])
        if 'fb' in post:
            fb = fb_search(string_name, count)
        if 'twitter' in post:
            twitter = twitter_search(string_name, count)
        return render(request, 'index.html', {'google': google,
                                              'vk': vk,
                                              'fb': fb,
                                              'twitter': twitter,
                                              'username': get_user(request).username})

    return render(request, 'index.html', {'username': get_user(request).username})


def receiver(request):
    name = request.GET.get('name')
    api = request.GET.get('api')
    link = request.GET.get('link')
    picture = request.GET.get('picture')
    if Friend.objects.filter(page=link).first():
        message = 'EXIST'
    else:
        user = request.user
        friend = Friend(name=name, api=api, page=link,
                        image=picture, user=user)
        friend.save()
        message = 'CREATED'
    return HttpResponse(message)
