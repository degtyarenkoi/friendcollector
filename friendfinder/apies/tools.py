

def get_sex(string):
    if int(string) == 0:
        return 'Gender not set'
    elif int(string) == 1:
        return 'Female'
    else:
        return  'Male'

def get_string_name(f_name, l_name):
    if f_name and l_name:
        return f_name + ' ' + l_name
    elif f_name and not l_name:
        return f_name
    elif not f_name and l_name:
        return l_name
    else:
        return None
