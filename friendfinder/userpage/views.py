from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.contrib.auth import get_user
from .models import Friend
#from django.contrib.auth.models import User
from user.models import MyUser
from django.template import RequestContext

# Create your views here.

def main(request):
    user = request.user
    if not user.is_anonymous():
        friends = Friend.objects.filter(user_id=user.id)
        return render_to_response('profile.html', {'friends':friends, 'user':user},RequestContext(request))
    else:
        return redirect('/user/login/')

def addfriend(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        api = request.POST.get('api')
        link = request.POST.get('link')
        picture = request.POST.get('picture')
    username = get_user(request).username
    user = MyUser.objects.get(username=username)
    user_id = user.id
    friend = Friend(name=name, api=api, page=link, image=picture)
    friend.user_id = user_id
    friend.save()
    return redirect('/userpage/')



def deletefriend(request, friend_id):
    friend = Friend.objects.get(id=friend_id)
    friend.delete()
    return redirect('/userpage/')
