from django.conf.urls import url, include
from apies import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^receiver/$', views.receiver),
]
